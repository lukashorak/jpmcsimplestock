package com.lh.simplestock;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

public class Market {
	private List<StockTrade> trades = new ArrayList<StockTrade>();

	private Map<String, StockItem> items = new HashMap<String, StockItem>();

	private Map<String, Integer> tickerPrices = new HashMap<String, Integer>();

	public Market() {

	}

	/**
	 * Record a trade, with timestamp, quantity of shares, buy or sell indicator
	 * and price
	 * 
	 * @param symbol
	 * @param timestamp
	 * @param volume
	 * @param type
	 *            Buy/Sell
	 * @param price
	 *            (in pennies)
	 */
	public void recordTrade(String symbol, Date timestamp, Long volume,
			TradeType type, Integer price) {

		StockTrade newTrade = new StockTrade(symbol, timestamp, volume, type,
				price);
		this.trades.add(newTrade);
		this.tickerPrices.put(symbol, price);
	}

	/**
	 * Calculate the dividend yield
	 * 
	 * @param symbol
	 * @return
	 */
	public Double getDividendYield(String symbol) {

		Integer tickerPrice = this.getTickerPrices().get(symbol);
		StockItem item = this.getItems().get(symbol);

		Double dividendYield = null;
		switch (item.getType()) {
		case Common: {
			dividendYield = (double) item.getLastDividend()
					/ (double) tickerPrice;
			break;
		}
		case Preferred: {
			dividendYield = ((double) item.getFixedDividend() * 0.01 * (double) item
					.getParValue()) / (double) tickerPrice;
			break;
		}
		}

		return dividendYield;
	}

	/**
	 * Calculate the P/E Ratio
	 * 
	 * @param symbol
	 * @return
	 */
	public Double getPeRatio(String symbol) {
		Integer tickerPrice = this.getTickerPrices().get(symbol);
		Integer dividend = this.getItems().get(symbol).getLastDividend();

		Double peRatio = (double) tickerPrice / (double) dividend;
		return peRatio;
	}

	/**
	 * Calculate Stock Price based on trades recorded in past 15 minutes
	 * 
	 * @param symbol
	 * @return
	 */
	public Double getStockPriceInLast15Minutes(String symbol) {

		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MINUTE, -15);

		Double upSum = 0.0;
		Double downSum = 0.0;
		List<StockTrade> inLimit = this
				.getTrades()
				.stream()
				.parallel()
				.filter(p -> {
					return symbol.equals(p.getSymbol())
							&& (p.getTimestamp().after(calendar.getTime()));
				}).collect(Collectors.toList());

		for (StockTrade s : inLimit) {
			upSum += (double) s.getPrice() * (double) s.getVolume();
			downSum += (double) s.getVolume();
		}

		Double stockPrice = upSum / downSum;
		return stockPrice;
	}

	/**
	 * Calculate the GBCE All Share Index using the geometric mean of prices for
	 * all stocks
	 * 
	 * @return
	 */
	public Double getGeometricMeanOfGBCE() {
		Double gMean = 1.0;

		int count = 0;
		synchronized (this.getTickerPrices()) {
			for (Entry<String, Integer> e : this.getTickerPrices().entrySet()) {
				count++;
				gMean *= (double) e.getValue();
			}
		}
		gMean = Math.pow(gMean, 1 / (double) count);

		return gMean;
	}

	public Map<String, StockItem> getItems() {
		return items;
	}

	public void setItems(Map<String, StockItem> items) {
		this.items = items;
	}

	public Map<String, Integer> getTickerPrices() {
		return tickerPrices;
	}

	public void setTickerPrices(Map<String, Integer> tickerPrices) {
		this.tickerPrices = tickerPrices;
	}

	public List<StockTrade> getTrades() {
		return trades;
	}

	public void setTrades(List<StockTrade> trades) {
		this.trades = trades;
	}
}