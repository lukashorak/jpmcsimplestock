package com.lh.simplestock;

public class StockItem {

	private String symbol;
	private StockType type;
	private Integer lastDividend;
	private Integer fixedDividend;
	private Integer parValue;

	public StockItem() {

	}

	/**
	 * 
	 * @param symbol
	 * @param type Common/Preffered
	 * @param lastDividend (in pennies)
	 * @param fixedDividend (in percentage)
	 * @param parValue (in pennies)
	 */
	public StockItem(String symbol, StockType type, Integer lastDividend,
			Integer fixedDividend, Integer parValue) {
		super();
		this.symbol = symbol;
		this.type = type;
		this.lastDividend = lastDividend;
		this.fixedDividend = fixedDividend;
		this.parValue = parValue;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public StockType getType() {
		return type;
	}

	public void setType(StockType type) {
		this.type = type;
	}

	public Integer getLastDividend() {
		return lastDividend;
	}

	public void setLastDividend(Integer lastDividend) {
		this.lastDividend = lastDividend;
	}

	public Integer getFixedDividend() {
		return fixedDividend;
	}

	public void setFixedDividend(Integer fixedDividend) {
		this.fixedDividend = fixedDividend;
	}

	public Integer getParValue() {
		return parValue;
	}

	public void setParValue(Integer parValue) {
		this.parValue = parValue;
	}

}