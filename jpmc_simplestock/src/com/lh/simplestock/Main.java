package com.lh.simplestock;

import com.lh.simplestock.test.MarketTest;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Market m = new Market();
		MarketTest t = new MarketTest();
		t.initMarket(m);
		
		System.out.println("GBCE  =" + m.getGeometricMeanOfGBCE());
		System.out.println("Price (TEA) ="
				+ m.getStockPriceInLast15Minutes("TEA"));
		System.out.println("Dividend Yield (POP) ="
				+ m.getDividendYield("POP"));
		System.out.println("Dividend Yield (GIN) ="
				+ m.getDividendYield("GIN"));
		System.out.println("PE Ratio (POP) ="
				+ m.getPeRatio("POP"));
	}

}
