package com.lh.simplestock.test;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import com.lh.simplestock.Market;
import com.lh.simplestock.StockItem;
import com.lh.simplestock.StockTrade;
import com.lh.simplestock.StockType;
import com.lh.simplestock.TradeType;

public class MarketTest {

	Market m;

	@Before
	public void setUp() {
		m = new Market();
		this.initMarket(m);
	}

	public void initMarket(Market m) {
		m.getItems().put("TEA", new StockItem("TEA", StockType.Common, 0, 0, 100));
		m.getItems().put("POP", new StockItem("POP", StockType.Common, 8, 0, 100));
		m.getItems().put("ALE", new StockItem("ALE", StockType.Common, 23, 0, 60));
		m.getItems().put("GIN", new StockItem("GIN", StockType.Preferred, 8, 2, 100));
		m.getItems().put("JOE", new StockItem("JOE", StockType.Common, 13, 0, 250));

		m.getTickerPrices().put("TEA", 101);
		m.getTickerPrices().put("POP", 111);
		m.getTickerPrices().put("ALE", 123);
		m.getTickerPrices().put("GIN", 145);
		m.getTickerPrices().put("JOE", 189);

		Date now = new Date();
		m.getTrades().add(new StockTrade("TEA", new Date(now.getTime() - 60000),
				100L, TradeType.Buy, 101));
		for (int i = 0; i < 15; i++) {
			m.getTrades().add(new StockTrade("TEA", new Date(now.getTime()
					- (i * 60000)), 100L, TradeType.Buy, 101 + i));
			m.getTrades().add(new StockTrade("POP", new Date(now.getTime()
					- (i * 60000)), 100L, TradeType.Buy, 111 + i));
			m.getTrades().add(new StockTrade("ALE", new Date(now.getTime()
					- (i * 60000)), 100L, TradeType.Buy, 123 + i));
			m.getTrades().add(new StockTrade("GIN", new Date(now.getTime()
					- (i * 60000)), 100L, TradeType.Buy, 145 + i));
			m.getTrades().add(new StockTrade("JOE", new Date(now.getTime()
					- (i * 60000)), 100L, TradeType.Buy, 189 + i));
		}
	}

	@Test
	public void testGBCE() {
		Double v1 = 130.45954336905092;
		Double v2 = m.getGeometricMeanOfGBCE();
		assertEquals(v1, v2);
	}

	@Test
	public void testPrice() {
		Double v1 = 107.5625;
		Double v2 = m.getStockPriceInLast15Minutes("TEA");
		assertEquals(v1, v2);
	}

	@Test
	public void testDividendCommon() {
		Double v1 = 0.07207207207207207;
		Double v2 = m.getDividendYield("POP");
		assertEquals(v1, v2);
	}

	@Test
	public void testDividendPreffered() {
		Double v1 = 0.013793103448275862;
		Double v2 = m.getDividendYield("GIN");
		assertEquals(v1, v2);
	}

	@Test
	public void testPeRatio() {
		Double v1 = 13.875;
		Double v2 = m.getPeRatio("POP");
		assertEquals(v1, v2);
	}

}
