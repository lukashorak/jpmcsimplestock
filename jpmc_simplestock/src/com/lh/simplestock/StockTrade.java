package com.lh.simplestock;

import java.util.Date;

public class StockTrade {
	private String symbol;
	private Date timestamp;
	private Long volume;
	private TradeType type;
	private Integer price;

	public StockTrade() {

	}

	/**
	 * 
	 * @param symbol
	 * @param timestamp
	 * @param volume
	 * @param type Buy/Sell
	 * @param price (in pennies)
	 */
	public StockTrade(String symbol, Date timestamp, Long volume,
			TradeType type, Integer price) {
		super();
		this.symbol = symbol;
		this.timestamp = timestamp;
		this.volume = volume;
		this.type = type;
		this.price = price;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public Long getVolume() {
		return volume;
	}

	public void setVolume(Long volume) {
		this.volume = volume;
	}

	public TradeType getType() {
		return type;
	}

	public void setType(TradeType type) {
		this.type = type;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

}
